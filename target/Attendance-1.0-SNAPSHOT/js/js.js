function form_submit(){
	document.getElementById("form").submit();
}
function form_reset(){
	document.getElementById("form").reset();
}
function reloadcode(){
    var verify=document.getElementById('safecode');
    verify.setAttribute('src','code.php?'+Math.random());
}

function checkAll(field) {
	var selectFlags = document.getElementsByName("selectFlag");
	for ( var i = 0; i < selectFlags.length; i++) {
		selectFlags[i].checked = field.checked;
	}
}


//删除字符串左边的空格
function ltrim(str) {
	if (str.length == 0)
		return (str);
	else {
		var idx = 0;
		while (str.charAt(idx).search(/\s/) == 0)
			idx++;
		return (str.substr(idx));
	}
}

// 删除字符串右边的空格
function rtrim(str) {
	if (str.length == 0)
		return (str);
	else {
		var idx = str.length - 1;
		while (str.charAt(idx).search(/\s/) == 0)
			idx--;
		return (str.substring(0, idx + 1));
	}
}

// 删除字符串左右两边的空格
function trim(str) {
	return (rtrim(ltrim(str)));
}