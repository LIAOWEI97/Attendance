<%@page import="com.util.SessionUtil"%>
<%@page import="com.util.Pagination"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.util.DbUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<script type="text/javascript" src="../js/js.js"></script>
<style type="text/css">
<!--
body {
	margin-left: 3px;
	margin-top: 0px;
	margin-right: 3px;
	margin-bottom: 0px;
}
.STYLE1 {
	color: #e1e2e3;
	font-size: 12px;
}
.STYLE6 {color: #000000; font-size: 12; }
.STYLE10 {color: #000000; font-size: 12px; }
.STYLE19 {
	color: #344b50;
	font-size: 12px;
}
.STYLE21 {
	font-size: 12px;
	color: #3b6375;
}
.STYLE22 {
	font-size: 12px;
	color: #295568;
}
-->
</style>
<script type="text/javascript">
	function add() {
		window.location.href = "stu_join_add.jsp";
	}
</script>
</head>
<%
	String account = SessionUtil.getUserInfo(request, response).getAccount();
	String role = SessionUtil.getUserInfo(request, response).getRole();
	String cid = request.getParameter("cid");
%>
<body>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="24" bgcolor="#353c44"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="6%" height="19" valign="bottom"><div align="center"><img src="../images/tb.gif" width="14" height="14" /></div></td>
                <td width="94%" valign="bottom"><span class="STYLE1"> 基本信息列表</span></td>
              </tr>
            </table></td>
            <td>
            	<div align="right">
            	<% 
            		if(role.equals("任课老师")){
            	%>
					<span class="STYLE1">
						<img src="../images/add.gif" width="10" height="10" /> 
						<span onclick="add();">添加</span>  &nbsp; 
					</span>
					<span class="STYLE1"> &nbsp;</span>
				<% 
            		}
				%>
				</div>
              </td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>
    <form name="form" id="form">
    <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#a8c7ce">
      <tr>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">编号</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">学号</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">课程号</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">上课时间</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">出勤情况</span></div></td>
      </tr>
     <%
     	String sql = "";
     	if("".equals(cid) && cid != null){
	     	sql = "select * from t_stu_join where cid = '" + cid + "' and account = '" + account + "'";
     	}else{
			sql = "select * from t_stu_join ";
     	}
		List<Map<String, Object>> list = DbUtil.find(sql);
		Map<String, Object> map = null;
		for (int i = 0; i < list.size(); i++) {
			map = (Map<String, Object>)list.get(i);
	%>
      <tr>
        <td height="20" bgcolor="#FFFFFF" class="STYLE6"><div align="center"><span class="STYLE19"><%=map.get("id") %></span></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%=map.get("account") %></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%=map.get("cid") %></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%=map.get("duetime") %></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19">
        	<div align="center">
       			<%=map.get("due") %>
        	</div>
        </td>
      </tr>
   <% 
	} 
   %> 
    </table>
    </form>
    </td>
  </tr>
</table>
</body>
</html>
