<%@page import="com.util.DbUtil"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<script type="text/javascript" src="../js/js.js"></script>
<script language="javascript" type="text/javascript" src="../My97DatePicker/WdatePicker.js"></script>
<style type="text/css">
<!--
body {
	margin-left: 3px;
	margin-top: 0px;
	margin-right: 3px;
	margin-bottom: 0px;
}
.STYLE1 {
	color: #e1e2e3;
	font-size: 12px;
}
.STYLE6 {color: #000000; font-size: 12; }
.STYLE10 {color: #000000; font-size: 12px; }
.STYLE19 {
	color: #344b50;
	font-size: 12px;
}
.STYLE21 {
	font-size: 12px;
	color: #3b6375;
}
.STYLE22 {
	font-size: 12px;
	color: #295568;
}
-->
</style>
</head>

<body>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="24" bgcolor="#353c44"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="6%" height="19" valign="bottom"><div align="center"><img src="../images/tb.gif" width="14" height="14" /></div></td>
                <td width="94%" valign="bottom"><span class="STYLE1"> 基本信息录入</span></td>
              </tr>
            </table></td>
            <td>
           	&nbsp;
            </td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>
    <form action="ClazzJoinServlet?method=add" method="post" name="form" id="form">
    <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#a8c7ce">
      <tr>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	班级名称
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          		<input type="text" name="name" />
          	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	&nbsp;
        	</div>
        </td>
      </tr>
      <tr>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	课程名称
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	<select name="cname">
          			<%
          			String sql_course = "select * from t_course";
					List<Map<String, Object>> list_course = DbUtil.find(sql_course);
					Map<String, Object> map_course = null;
					for (int i = 0; i < list_course.size(); i++) {
						map_course = (Map<String, Object>)list_course.get(i);
          			%>
          			<option value="<%=map_course.get("name")%>"><%=map_course.get("name")%></option>
          			<% 
					}
          			%>
          		</select>
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	&nbsp;
        	</div>
        </td>
      </tr>
      <tr>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	考勤时间
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	<input name="duetime" class="Wdate" type="text" id="d15" onFocus="WdatePicker({isShowClear:false,readOnly:true})"/>
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	&nbsp;
        	</div>
        </td>
      </tr>
      <tr>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	迟到人数
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          		<input type="text" name="num1" />
          	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	&nbsp;
        	</div>
        </td>
      </tr>
      <tr>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	旷课人数
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          		<input type="text" name="num2" />
          	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	&nbsp;
        	</div>
        </td>
      </tr>
      <tr>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	出勤人数
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          		<input type="text" name="num3" />
          	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	&nbsp;
        	</div>
        </td>
      </tr>
      <tr>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10" colspan="2">
        	<div align="center">
          	<input type="button" value="提交" onclick="form_submit();"/>&nbsp;&nbsp;
          	<input type="button" value="返回" onclick="history.go(-1);" />
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	&nbsp;
        	</div>
        </td>
      </tr>
    </table>
    </form>
    </td>
  </tr>
</table>
</body>
</html>
