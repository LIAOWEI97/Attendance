<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<script type="text/javascript" src="../js/js.js"></script>
<style type="text/css">
<!--
body {
	margin-left: 3px;
	margin-top: 0px;
	margin-right: 3px;
	margin-bottom: 0px;
}
.STYLE1 {
	color: #e1e2e3;
	font-size: 12px;
}
.STYLE6 {color: #000000; font-size: 12px; }
.STYLE10 {color: #000000; font-size: 12px; }
.STYLE19 {
	color: #344b50;
	font-size: 12px;
}
.STYLE21 {
	font-size: 12px;
	color: #3b6375;
}
.STYLE22 {
	font-size: 12px;
	color: #295568;
}
-->
</style>
<script type="text/javascript">
	function add() {
		window.location.href = "stu_leave_add.jsp";
	}
</script>
</head>
<%
	String account = (String) session.getAttribute("userName");
	String role = (String) session.getAttribute("userRole");
%>
<body>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="24" bgcolor="#353c44"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="6%" height="19" valign="bottom"><div align="center"><img src="../images/tb.gif" width="14" height="14" /></div></td>
                <td width="94%" valign="bottom"><span class="STYLE1"> 基本信息列表</span></td>
              </tr>
            </table></td>
            <td>
            	<div align="right">
				<% 
					if(role.equals("学生")){
				%>	
					<span class="STYLE1">
						<img src="../images/add.gif" width="10" height="10" /> 
						<span onclick="add();">请假</span>  &nbsp; 
					</span>
					<span class="STYLE1"> &nbsp;</span>
				<% 
					}
				%>
				</div>
              </td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>
    <form name="form" id="form">
    <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#a8c7ce">
      <tr>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">编号</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">请假人</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">请假时间</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">开始时间</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">请假原因</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">请假天数</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">状态</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">处理时间</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">处理人</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">处理结果</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">操作</span></div></td>
      </tr>
    <%-- <%
     	String sql = "";
      	if(role.equals("学生")){
			sql = "select * from t_stu_leave where account = '" + account + "'";
      	}else{
			sql = "select * from t_stu_leave";
      	}

		List<Map<String, Object>> list = DbUtil.find(sql);
		Map<String, Object> map = null;
		for (int i = 0; i < list.size(); i++) {
			map = (Map<String, Object>)list.get(i);
	%>--%>
      <tr>
        <td height="20" bgcolor="#FFFFFF" class="STYLE6"><div align="center"><span class="STYLE19"><%--<%=map.get("id") %>--%></span></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%--<%=map.get("account") %>--%></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%--<%=map.get("pubtime") %>--%></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><<%--%=map.get("starttime") %>--%></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%--<%=map.get("reason") %>--%></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%--<%=map.get("num") %>--%></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%--<%=map.get("status") %>--%></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%--<%=map.get("duetime") %>--%></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%--<%=map.get("duename") %>--%></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%--<%=map.get("result") %>--%></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19">
        	<div align="center">
        		<% 
        			if(role.equals("辅导员")){
        		%>
        			<a href="stu_leave_modify.jsp?id=<%--<%=map.get("id")%>--%>">审批</a>
        		<% 
        			}else{
        		%>
        		--
        		<%		
        			}
        		%>
        	</div>
        </td>
      </tr>
  <%-- <%
	}
   %> --%>
    </table>
    </form>
    </td>
  </tr>
</table>
</body>
</html>
