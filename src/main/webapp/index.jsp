<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>学生考勤管理系统</title>
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<script type="text/javascript" src="js/js.js"></script>
</head>
<body>
<div id="top">
	<div style="border: 0px solid red;"><br /><br /><br /><br /><h1>学生考勤管理系统</h1></div>
</div>
<form id="form" name="form" action="/login" method="post">
  <div id="center">
    <div id="center_left"></div>
    <div id="center_middle">
      <div class="user">
        <label>账 号：
        <input type="text" name="userAccount" id="userName" value=""/>
        </label>
      </div>
      <div class="user">
        <label>密　码：
        <input type="password" name="userPassword" id="userPassword" value=""/>
        </label>
      </div>
      <div class="chknumber">
        <label>角　色：
        <select name="role">
        	<option value="1">学生</option>
        	<option value="2">任课老师</option>
        	<option value="3">辅导员</option>
        	<option value="4">系统管理员</option>
        </select>
        </label>
        <!-- <img src="images/checkcode.png" id="safecode" /> -->
      </div>
    </div>
    <div id="center_middle_right">
    </div>
    <div id="center_submit">
      <div class="button"> <img src="images/dl.gif" width="57" height="20" onclick="form_submit()" /> </div>
      <div class="button"> <img src="images/cz.gif" width="57" height="20" onclick="form_reset()"/> </div>
    </div>
    <div id="center_right">
    </div>
  </div>
</form>
<div id="footer" style="text-align: center;">
	<br/>
</div>
</body>
</html>
