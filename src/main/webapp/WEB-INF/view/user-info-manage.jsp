<%@ page import="java.util.List" %>
<%@ page import="com.attendance.bean.UserInfo" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<script type="text/javascript" src="../js/js.js"></script>
<style type="text/css">
<!--
body {
	margin-left: 3px;
	margin-top: 0px;
	margin-right: 3px;
	margin-bottom: 0px;
}
.STYLE1 {
	color: #e1e2e3;
	font-size: 12px;
}
.STYLE6 {color: #000000; font-size: 12px; }
.STYLE10 {color: #000000; font-size: 12px; }
.STYLE19 {
	color: #344b50;
	font-size: 12px;
}
.STYLE21 {
	font-size: 12px;
	color: #3b6375;
}
.STYLE22 {
	font-size: 12px;
	color: #295568;
}
-->
</style>
<script type="text/javascript">
	function add() {
		window.location.href = "/api/userInfoAdd";
	}
	function del() {
		var selectFlags  = document.getElementsByName("selectFlag");
		var flag = false;
		for (var i=0; i < selectFlags.length; i++) {
			if (selectFlags[i].checked) {
				flag = true;
				break;
			}
		}
		if (!flag) {
			alert("请选择需要删除的记录！");
			return;
		}
		if (window.confirm("确认删除吗？")) {
			with (document.getElementById("form")) {
				action="/api/userInfoDel?" + selectFlags;
				method="get";
				submit();
			}
		}
	}
	function modify() {
		var selectFlags = document.getElementsByName("selectFlag");
		var count = 0;
		var j = 0;
		for ( var i = 0; i < selectFlags.length; i++) {
			if (selectFlags[i].checked) {
				j = i;
				count++;
			}
		}
		if (count == 0) {
			alert("请选择需要修改的记录！");
			return;
		}
		if (count > 1) {
			alert("一次只能修改一条记录！");
			return;
		}
		window.location.href = "/api/userInfoModify?id=" + selectFlags[j].value;
	}
</script>
</head>

<body>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="24" bgcolor="#353c44"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="6%" height="19" valign="bottom"><div align="center"><img src="../images/tb.gif" width="14" height="14" /></div></td>
                <td width="94%" valign="bottom"><span class="STYLE1"> 基本信息列表</span></td>
              </tr>
            </table></td>
            <td>
            	<jsp:include page="top_banner.jsp" />
              </td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>
    <form name="form" id="form">
    <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#a8c7ce">
      <tr>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10"><div align="center">
          &nbsp;
        </div></td>
        <td width="10%" height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">编号</span></div></td>
        <td width="15%" height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">帐号</span></div></td>
        <td width="14%" height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">密码</span></div></td>
        <td width="14%" height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">角色</span></div></td>
        <td width="14%" height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">姓名</span></div></td>
        <td width="14%" height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">性别</span></div></td>
        <td width="14%" height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">职称</span></div></td>
      </tr>
      <%
        List<UserInfo> userList = (List<UserInfo>)request.getAttribute("userInfoList");
		for (int i = 0; i < userList.size(); i++) {
			UserInfo user = userList.get(i);
	%>
      <tr>
        <td height="20" bgcolor="#FFFFFF"><div align="center">
          <input type="checkbox" name="selectFlag" value="<%=user.getUserId()%>" id="checkbox2" />
        </div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE6"><div align="center"><span class="STYLE19"><%=user.getUserId()%></span></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%=user.getUserAccount() %></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%=user.getUserPassword() %></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%=user.getUserRole() %></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%=user.getUserName() %></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%=user.getUserGender() %></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%=user.getPositional() %></div></td>
      </tr>
   <% 
	} 
   %> 
    </table>
    </form>
    </td>
  </tr>
  <tr>
    <td height="30"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="40%"><div align="left"><span class="STYLE22">&nbsp;&nbsp;&nbsp;</span></div></td>
        <td width="60%"><table width="312" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td width="49"><div align="center"><!-- <img src="../images/main_54.gif" width="40" height="15" /> --></div></td>
            <td width="49"><div align="center"><!-- <img src="../images/main_56.gif" width="45" height="15" /> --></div></td>
            <td width="49"><div align="center"><!-- <img src="../images/main_58.gif" width="45" height="15" /> --></div></td>
            <td width="49"><div align="center"><!-- <img src="../images/main_60.gif" width="40" height="15" /> --></div></td>
            <td width="37" class="STYLE22"><!-- <div align="center">转到</div> --></td>
            <td width="22"><div align="center">
              <!-- <input type="text" name="textfield" id="textfield"  style="width:20px; height:12px; font-size:12px; border:solid 1px #7aaebd;"/> -->
            </div></td>
            <td width="22" class="STYLE22"><!-- <div align="center">页</div> --></td>
            <td width="35"><!-- <img src="../images/main_62.gif" width="26" height="15" /> --></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
