<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<script type="text/javascript" src="../js/js.js"></script>
<style type="text/css">
<!--
body {
	margin-left: 3px;
	margin-top: 0px;
	margin-right: 3px;
	margin-bottom: 0px;
}
.STYLE1 {
	color: #e1e2e3;
	font-size: 12px;
}
.STYLE6 {color: #000000; font-size: 12px; }
.STYLE10 {color: #000000; font-size: 12px; }
.STYLE19 {
	color: #344b50;
	font-size: 12px;
}
.STYLE21 {
	font-size: 12px;
	color: #3b6375;
}
.STYLE22 {
	font-size: 12px;
	color: #295568;
}
-->
</style>
<script type="text/javascript">
	function validate(userfield) {
		if (trim(userfield.value).length != 0) {
			var xmlHttpRequest = null;
			var url = "../UserInfoIsExistServlet?id=" + userfield.value;
			var usermsg = document.getElementById("usermsg");
			if (window.XMLHttpRequest) {//表示当前浏览器不是IE
				xmlHttpRequest = new XMLHttpRequest();
			} else if (window.ActiveXObject) {
				xmlHttpRequest = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlHttpRequest.open("GET", url, true);//设置请求方式为GET，设置请求的URL，设置为异步提交
			xmlHttpRequest.onreadystatechange = function(){//将方法地址复制给onreadystatechange属性
				if(xmlHttpRequest.readyState == 4){//Ajax引擎状态为成功
					if(xmlHttpRequest.status == 200){//HTTP协议状态为成功
						if(trim(xmlHttpRequest.responseText) != ""){
							usermsg.innerHTML = "<font color='red'>" + trim(xmlHttpRequest.responseText) + "</font>";
							userfield.focus();
						}else{
							usermsg.innerHTML = "恭喜您，用户名可以使用。 ";
						}
					}else{
						alert("请求失败，错误码=" + xmlHttp.status);
					}
				}
			};
			xmlHttpRequest.send(null);//将设置信息发送到Ajax引擎
		}else{
			usermsg.innerHTML = "";
		}
	}
</script>
</head>

<body>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="24" bgcolor="#353c44"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="6%" height="19" valign="bottom"><div align="center"><img src="../images/tb.gif" width="14" height="14" /></div></td>
                <td width="94%" valign="bottom"><span class="STYLE1"> 基本信息录入</span></td>
              </tr>
            </table></td>
            <td>
           	&nbsp;
            </td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>
    <form action="/api/userInfoAdd" method="post" name="form" id="form">
    <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#a8c7ce">
      <tr>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	帐号
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	<input type="text" name="userAccount" id="userAccount" maxlength="10" onblur="validate(this)"/>
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          		<span id="usermsg"></span>&nbsp;
        	</div>
        </td>
      </tr>
      <tr>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	密码
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	<input type="text" name="userPassword" id="userPassword" maxlength="10" value=""/>
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	&nbsp;
        	</div>
        </td>
      </tr>
      <tr>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	角色
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          		<select name="userRole">
          			<option value="学生">学生</option>
		        	<option value="辅导员">辅导员</option>
		        	<option value="任课老师">任课老师</option>
		            <option value="管理员">管理员</option>
          		</select>
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	&nbsp;
        	</div>
        </td>
      </tr>
      <tr>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	姓名
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          		<input type="text" name="userName" id="userName" maxlength="10" value=""/>
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	&nbsp;
        	</div>
        </td>
      </tr>
      <tr>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	性别
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          		<input type="radio" name="userGender" id="userGender1" maxlength="10" value="1" checked="checked"/>男
          		<input type="radio" name="userGender" id="userGender" maxlength="10" value="2"/>女
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	&nbsp;
        	</div>
        </td>
      </tr>
      <tr>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	职称
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          		<input type="text" name="title" id="title" maxlength="10" value=""/>
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	&nbsp;
        	</div>
        </td>
      </tr>
      <tr>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10" colspan="2">
        	<div align="center">
          	<input type="button" value="提交" onclick="form_submit();"/>&nbsp;&nbsp;
          	<input type="button" value="返回" onclick="history.go(-1);" />
        	</div>
        </td>
        <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10">
        	<div align="center">
          	&nbsp;
        	</div>
        </td>
      </tr>
    </table>
    </form>
    </td>
  </tr>
</table>
</body>
</html>
