<%@ page import="com.attendance.bean.ClassJoin" %>
<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<script type="text/javascript" src="../js/js.js"></script>
<style type="text/css">
<!--
body {
	margin-left: 3px;
	margin-top: 0px;
	margin-right: 3px;
	margin-bottom: 0px;
}
.STYLE1 {
	color: #e1e2e3;
	font-size: 12px;
}
.STYLE6 {color: #000000; font-size: 12px; }
.STYLE10 {color: #000000; font-size: 12px; }
.STYLE19 {
	color: #344b50;
	font-size: 12px;
}
.STYLE21 {
	font-size: 12px;
	color: #3b6375;
}
.STYLE22 {
	font-size: 12px;
	color: #295568;
}
-->
</style>
<script type="text/javascript">
	function add() {
		window.location.href = "/api/classJoinAdd";
	}
	function del() {
		var classJoinId = document.getElementsByName("classJoinId");
		var flag = false;
		for (var i=0; i < classJoinId.length; i++) {
			if (classJoinId[i].checked) {
				flag = true;
				break;
			}
		}
		if (!flag) {
			alert("请选择需要删除的记录！");
			return;
		}
		if (window.confirm("确认删除吗？")) {
			with (document.getElementById("form")) {
				action="/api/ClazzJoinDel";
				method="post";
				submit();
			}
		}
	}
	function modify() {
		var selectFlags = document.getElementsByName("selectFlag");
		var count = 0;
		var j = 0;
		for ( var i = 0; i < selectFlags.length; i++) {
			if (selectFlags[i].checked) {
				j = i;
				count++;
			}
		}
		if (count == 0) {
			alert("请选择需要修改的记录！");
			return;
		}
		if (count > 1) {
			alert("一次只能修改一条记录！");
			return;
		}
		window.location.href = "clazz_join_modify.jsp?id=" + selectFlags[j].value;
	}
</script>
</head>
<body>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="24" bgcolor="#353c44"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="6%" height="19" valign="bottom"><div align="center"><img src="../images/tb.gif" width="14" height="14" /></div></td>
                <td width="94%" valign="bottom"><span class="STYLE1"> 基本信息列表</span></td>
              </tr>
            </table></td>
            <td>
            	<div align="right">
					<div align="right">
						<span class="STYLE1">
							<input type="checkbox" name="ifAll" onClick="checkAll(this)" id="checkbox11" />
							<span>全选</span>      &nbsp;&nbsp;
							<img src="../images/add.gif" width="10" height="10" /> 
							<span onclick="add();">添加</span>  &nbsp; 
							<img src="../images/del.gif" width="10" height="10" /> 
							<span onclick="del();">删除</span>    &nbsp;&nbsp;
						</span>
						<span class="STYLE1"> &nbsp;</span>
					</div>
				</div>
              </td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>
    <form name="form" id="form">
    <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#a8c7ce">
      <tr>
       <td width="4%" height="20" bgcolor="d3eaef" class="STYLE10"><div align="center">
          &nbsp;
        </div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">编号</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">班级名称</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">课程名称</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">考勤时间</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">迟到人数</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">旷课人数</span></div></td>
        <td height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">出勤人数</span></div></td>
      </tr>
        <%
            List<ClassJoin> classJoins = (List<ClassJoin>)request.getAttribute("classJoins");
            for (int i = 0; i < classJoins.size(); i++) {
                ClassJoin classJoin = classJoins.get(i);
        %>
      <tr>
      	<td height="20" bgcolor="#FFFFFF"><div align="center">
          <input type="checkbox" name="classJoinId" value="<%=classJoin.getClassJoinId()%>" id="checkbox2" />
        </div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE6"><div align="center"><span class="STYLE19"><%=classJoin.getClassJoinId() %></span></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%=classJoin.getClassName()%></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%=classJoin.getCourseName() %></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%=classJoin.getAttendanceTime() %></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%=classJoin.getLateArrivals()%></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%=classJoin.getTruantNumber() %></div></td>
        <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><%=classJoin.getAttendanceNumber()%></div></td>
      </tr>
   <%
	} 
   %>
    </table>
    </form>
    </td>
  </tr>
</table>
</body>
</html>
