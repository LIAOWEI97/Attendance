<%--<%@page import="com.util.SessionUtil"%>
<%@page import="com.domain.UserInfo"%>--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<script type="text/javascript" src="../../js/jquery.js"></script>
<script type="text/javascript" src="../../js/chili-1.7.pack.js"></script>
<script type="text/javascript" src="../../js/jquery.easing.js"></script>
<script type="text/javascript" src="../../js/jquery.dimensions.js"></script>
<script type="text/javascript" src="../../js/jquery.accordion.js"></script>
<script language="javascript">
	jQuery().ready(function(){
		jQuery('#navigation').accordion({
			header: '.head',
			navigation1: true, 
			event: 'click',
			fillSpace: true,
			animated: 'bounceslide'
		});
	});
</script>
<style type="text/css">
<!--
body {
	margin:0px;
	padding:0px;
	font-size: 12px;
}
#navigation {
	margin:0px;
	padding:0px;
	width:147px;
}
#navigation a.head {
	cursor:pointer;
	background:url(../../images/main_34.gif) no-repeat scroll;
	display:block;
	font-weight:bold;
	margin:0px;
	padding:5px 0 5px;
	text-align:center;
	font-size:12px;
	text-decoration:none;
}
#navigation ul {
	border-width:0px;
	margin:0px;
	padding:0px;
	text-indent:0px;
}
#navigation li {
	list-style:none; display:inline;
}
#navigation li li a {
	display:block;
	font-size:12px;
	text-decoration: none;
	text-align:center;
	padding:3px;
}
#navigation li li a:hover {
	background:url(../../images/tab_bg.gif) repeat-x;
		border:solid 1px #adb9c2;
}
-->
</style>
</head>
<body>
<%
	String role = (String) session.getAttribute("userRole");
%>
<div>
  <ul id="navigation">
	<li>
		<a class="head">个人信息管理</a>
	      <ul>
	        <li><a href="/api/passwordModify" target="rightFrame">密码修改</a></li>
	    <%
  	    	if(role.equals("学生")){
	    %>
 	    <li><a href="/api/studentInfo" target="rightFrame">信息维护</a></li>
	    <%
	    	}
  	    	if(role.equals("任课老师")){
	    %>
 	        <li><a href="/api/teacherInfo" target="rightFrame">信息维护</a></li>
	    <%
    	}
	    %>
	      </ul>
    </li>
<%
   	if(role.equals("学生") || role.equals("辅导员") || role.equals("院系领导") || role.equals("校领导")){
%>
    <li> <a class="head">学生请假管理</a>
      <ul>
        <li><a href="/api/stuLeaveManage" target="rightFrame">请假记录</a></li>
      </ul>
    </li>
<%
   	}
%>
<%
   	if(role.equals("任课老师") || role.equals("校领导")){
%>

    <li> <a class="head">班级出勤记录</a>
      <ul>
        <li><a href="/api/clazzJoinManage" target="rightFrame">查看班级出勤记录</a></li>
      </ul>
    </li>
     <li> <a class="head">学生出勤记录</a>
      <ul>
        <li><a href="/api/stuJoinManage" target="rightFrame">查看学生出勤记录</a></li>
      </ul>
    </li>
<%
   	}
%>

    <li> <a class="head">课程信息管理</a>
      <ul>
        <li><a href="/api/courseManage" target="rightFrame">课程查看</a></li>
      </ul>
    </li>
<%
   	if(role.equals("管理员")){
%>
    <li> <a class="head">用户信息管理</a>
      <ul>
        <li><a href="/api/userInfoManage" target="rightFrame">查看用户</a></li>
        <li><a href="/api/userInfoAdd" target="rightFrame">添加用户</a></li>
      </ul>
    </li>
    <li> <a class="head">学生信息管理</a>
      <ul>
        <li><a href="/api/studentManage" target="rightFrame">学生查看</a></li>
      </ul>
    </li>
    <li> <a class="head">教师信息管理</a>
      <ul>
        <li><a href="/api/teacherManage" target="rightFrame">教师查看</a></li>
      </ul>
    </li>
<%
   	}
%>
  </ul>
</div>
</body>
</html>