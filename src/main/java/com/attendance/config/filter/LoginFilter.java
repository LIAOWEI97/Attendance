package com.attendance.config.filter;

import com.attendance.utils.ChannelContext;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        if (ChannelContext.getUser() == null) {
            httpServletResponse.sendError(403, "请先登录!");
            return;
        }
        chain.doFilter(httpServletRequest, httpServletResponse);
    }
}