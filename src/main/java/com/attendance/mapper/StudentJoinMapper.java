package com.attendance.mapper;

import com.attendance.bean.StudentJoin;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface StudentJoinMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table student_join
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    int deleteByPrimaryKey(Integer joinId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table student_join
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    int insert(StudentJoin record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table student_join
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    int insertSelective(StudentJoin record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table student_join
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    StudentJoin selectByPrimaryKey(Integer joinId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table student_join
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    int updateByPrimaryKeySelective(StudentJoin record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table student_join
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    int updateByPrimaryKey(StudentJoin record);

    List<StudentJoin> selectByExample();
}