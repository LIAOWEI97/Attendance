package com.attendance.utils;

import com.attendance.bean.UserInfo;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.concurrent.ConcurrentHashMap;

public class ChannelContext {
    private static ConcurrentHashMap<String, UserInfo> users = new ConcurrentHashMap<>();

    public static UserInfo getUser() {
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attrs.getRequest();
        return users.get(request.getSession().getAttribute("userAccount"));
    }

    public static void addUser(UserInfo u) {
        getSession().setAttribute("userAccount", u.getUserAccount());
        getSession().setAttribute("userName", u.getUserName());

        getSession().setAttribute("userRole", u.getUserRole().trim());
        users.put(u.getUserAccount(), u);
    }

    public static HttpSession getSession(){
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attrs.getRequest();
        HttpSession session = request.getSession();

        return session;
    }
}
