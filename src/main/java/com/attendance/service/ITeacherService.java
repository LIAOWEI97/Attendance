package com.attendance.service;

import com.attendance.bean.TeacherInfo;

import java.util.List;

public interface ITeacherService {
    int deleteByPrimaryKey(Integer teacherId);

    int insert(TeacherInfo record);

    int insertSelective(TeacherInfo record);

    TeacherInfo selectByPrimaryKey(Integer teacherId);

    int updateByPrimaryKeySelective(TeacherInfo record);

    int updateByPrimaryKey(TeacherInfo record);

    List<TeacherInfo> selectByExample();
}
