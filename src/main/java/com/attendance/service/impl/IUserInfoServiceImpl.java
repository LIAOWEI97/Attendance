package com.attendance.service.impl;

import com.attendance.bean.UserInfo;
import com.attendance.mapper.UserInfoMapper;
import com.attendance.service.IUserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IUserInfoServiceImpl implements IUserInfoService {
    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public int deleteByPrimaryKey(Integer userId) {
        return userInfoMapper.deleteByPrimaryKey(userId);
    }

    @Override
    public int insert(UserInfo record) {
        return userInfoMapper.insert(record);
    }

    @Override
    public int insertSelective(UserInfo record) {
        return userInfoMapper.insertSelective(record);
    }

    @Override
    public UserInfo selectByPrimaryKey(Integer userId) {
        return userInfoMapper.selectByPrimaryKey(userId);
    }

    @Override
    public int updateByPrimaryKeySelective(UserInfo record) {
        return userInfoMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(UserInfo record) {
        return userInfoMapper.updateByPrimaryKey(record);
    }

    @Override
    public UserInfo selectByAccountAndPassword(UserInfo record) {
        return userInfoMapper.selectByAccountAndPassword(record);
    }

    @Override
    public List<UserInfo> selectByExample() {
        return userInfoMapper.selectByExample();
    }

    @Override
    public List<UserInfo> selectByRole(String role) {
        return userInfoMapper.selectByRole(role);
    }
}
