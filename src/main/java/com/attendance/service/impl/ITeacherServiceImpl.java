package com.attendance.service.impl;

import com.attendance.bean.TeacherInfo;
import com.attendance.mapper.TeacherInfoMapper;
import com.attendance.service.ITeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ITeacherServiceImpl implements ITeacherService {
    @Autowired
    private TeacherInfoMapper teacherInfoMapper;
    @Override
    public int deleteByPrimaryKey(Integer teacherId) {
        return teacherInfoMapper.deleteByPrimaryKey(teacherId);
    }

    @Override
    public int insert(TeacherInfo record) {
        return teacherInfoMapper.insert(record);
    }

    @Override
    public int insertSelective(TeacherInfo record) {
        return teacherInfoMapper.insertSelective(record);
    }

    @Override
    public TeacherInfo selectByPrimaryKey(Integer teacherId) {
        return teacherInfoMapper.selectByPrimaryKey(teacherId);
    }

    @Override
    public int updateByPrimaryKeySelective(TeacherInfo record) {
        return teacherInfoMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(TeacherInfo record) {
        return teacherInfoMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<TeacherInfo> selectByExample() {
        return teacherInfoMapper.selectByExample();
    }
}
