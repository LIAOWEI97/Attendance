package com.attendance.service.impl;

import com.attendance.bean.ClassJoin;
import com.attendance.mapper.ClassJoinMapper;
import com.attendance.service.IClassJoinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IClassJoinServiceImpl implements IClassJoinService {
    @Autowired
    private ClassJoinMapper classJoinMapper;
    @Override
    public int deleteByPrimaryKey(Integer courseId) {
        return classJoinMapper.deleteByPrimaryKey(courseId);
    }

    @Override
    public int insert(ClassJoin record) {
        return classJoinMapper.insert(record);
    }

    @Override
    public int insertSelective(ClassJoin record) {
        return classJoinMapper.insertSelective(record);
    }

    @Override
    public ClassJoin selectByPrimaryKey(Integer courseId) {
        return classJoinMapper.selectByPrimaryKey(courseId);
    }

    @Override
    public int updateByPrimaryKeySelective(ClassJoin record) {
        return classJoinMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(ClassJoin record) {
        return classJoinMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<ClassJoin> selectByExample() {
        return classJoinMapper.selectByExample();
    }
}
