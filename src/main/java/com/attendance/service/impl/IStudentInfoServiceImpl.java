package com.attendance.service.impl;

import com.attendance.bean.StudentInfo;
import com.attendance.bean.UserInfo;
import com.attendance.mapper.StudentInfoMapper;
import com.attendance.service.IStudentInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IStudentInfoServiceImpl implements IStudentInfoService {
    @Autowired
    private StudentInfoMapper studentInfoMapper;

    @Override
    public int deleteByPrimaryKey(Integer studentId) {
        return studentInfoMapper.deleteByPrimaryKey(studentId);
    }

    @Override
    public int insert(StudentInfo record) {
        return studentInfoMapper.insert(record);
    }

    @Override
    public int insertSelective(StudentInfo record) {
        return studentInfoMapper.insertSelective(record);
    }

    @Override
    public StudentInfo selectByPrimaryKey(Integer studentId) {
        return studentInfoMapper.selectByPrimaryKey(studentId);
    }

    @Override
    public int updateByPrimaryKeySelective(StudentInfo record) {
        return studentInfoMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(StudentInfo record) {
        return studentInfoMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<StudentInfo> selectByExample() {
        return studentInfoMapper.selectByExample();
    }
}
