package com.attendance.service.impl;

import com.attendance.bean.StudentLeave;
import com.attendance.bean.TeacherInfo;
import com.attendance.mapper.StudentLeaveMapper;
import com.attendance.mapper.TeacherInfoMapper;
import com.attendance.service.IStudentLeaveService;
import com.attendance.service.ITeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IStudentLeaveServiceImpl implements IStudentLeaveService {
    @Autowired
    private StudentLeaveMapper studentLeaveMapper;

    @Override
    public int deleteByPrimaryKey(Integer leaveId) {
        return studentLeaveMapper.deleteByPrimaryKey(leaveId);
    }

    @Override
    public int insert(StudentLeave record) {
        return studentLeaveMapper.insert(record);
    }

    @Override
    public int insertSelective(StudentLeave record) {
        return studentLeaveMapper.insertSelective(record);
    }

    @Override
    public StudentLeave selectByPrimaryKey(Integer leaveId) {
        return studentLeaveMapper.selectByPrimaryKey(leaveId);
    }

    @Override
    public int updateByPrimaryKeySelective(StudentLeave record) {
        return studentLeaveMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(StudentLeave record) {
        return studentLeaveMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<StudentLeave> selectByExample() {
        return studentLeaveMapper.selectByExample();
    }
}
