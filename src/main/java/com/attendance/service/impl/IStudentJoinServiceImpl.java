package com.attendance.service.impl;

import com.attendance.bean.StudentJoin;
import com.attendance.mapper.StudentJoinMapper;
import com.attendance.service.IStudentJoinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IStudentJoinServiceImpl implements IStudentJoinService {
    @Autowired
    private StudentJoinMapper studentJoinMapper;

    @Override
    public int deleteByPrimaryKey(Integer joinId) {
        return studentJoinMapper.deleteByPrimaryKey(joinId);
    }

    @Override
    public int insert(StudentJoin record) {
        return studentJoinMapper.insert(record);
    }

    @Override
    public int insertSelective(StudentJoin record) {
        return studentJoinMapper.insertSelective(record);
    }

    @Override
    public StudentJoin selectByPrimaryKey(Integer joinId) {
        return studentJoinMapper.selectByPrimaryKey(joinId);
    }

    @Override
    public int updateByPrimaryKeySelective(StudentJoin record) {
        return studentJoinMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(StudentJoin record) {
        return studentJoinMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<StudentJoin> selectByExample() {
        return studentJoinMapper.selectByExample();
    }
}
