package com.attendance.service;

import com.attendance.bean.StudentLeave;
import com.attendance.bean.TeacherInfo;

import java.util.List;

public interface IStudentLeaveService {
    int deleteByPrimaryKey(Integer leaveId);

    int insert(StudentLeave record);

    int insertSelective(StudentLeave record);

    StudentLeave selectByPrimaryKey(Integer leaveId);

    int updateByPrimaryKeySelective(StudentLeave record);

    int updateByPrimaryKey(StudentLeave record);

    List<StudentLeave> selectByExample();
}
