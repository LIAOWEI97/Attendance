package com.attendance.service;

import com.attendance.bean.ClassJoin;

import java.util.List;

public interface IClassJoinService {
    int deleteByPrimaryKey(Integer classJoinId);

    int insert(ClassJoin record);

    int insertSelective(ClassJoin record);

    ClassJoin selectByPrimaryKey(Integer classJoinId);

    int updateByPrimaryKeySelective(ClassJoin record);

    int updateByPrimaryKey(ClassJoin record);

    List<ClassJoin> selectByExample();
}
