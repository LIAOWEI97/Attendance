package com.attendance.service;

import com.attendance.bean.Course;
import com.attendance.bean.TeacherInfo;

import java.util.List;

public interface ICourseService {
    int deleteByPrimaryKey(Integer courseId);

    int insert(Course record);

    int insertSelective(Course record);

    Course selectByPrimaryKey(Integer courseId);

    int updateByPrimaryKeySelective(Course record);

    int updateByPrimaryKey(Course record);

    List<Course> selectByExample();
}
