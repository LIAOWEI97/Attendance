package com.attendance.service;

import com.attendance.bean.StudentInfo;
import com.attendance.bean.UserInfo;

import java.util.List;

public interface IStudentInfoService {
    int deleteByPrimaryKey(Integer studentId);

    int insert(StudentInfo record);

    int insertSelective(StudentInfo record);

    StudentInfo selectByPrimaryKey(Integer studentId);

    int updateByPrimaryKeySelective(StudentInfo record);

    int updateByPrimaryKey(StudentInfo record);

    List<StudentInfo> selectByExample();
}
