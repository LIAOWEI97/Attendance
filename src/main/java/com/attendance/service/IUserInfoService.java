package com.attendance.service;

import com.attendance.bean.UserInfo;
import org.springframework.stereotype.Service;

import java.util.List;

public interface IUserInfoService {
    int deleteByPrimaryKey(Integer userId);

    int insert(UserInfo record);

    int insertSelective(UserInfo record);

    UserInfo selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(UserInfo record);

    int updateByPrimaryKey(UserInfo record);

    UserInfo selectByAccountAndPassword(UserInfo record);

    List<UserInfo> selectByExample();

    List<UserInfo> selectByRole(String role);
}
