package com.attendance.controller;

import com.attendance.bean.Course;
import com.attendance.bean.UserInfo;
import com.attendance.service.ICourseService;
import com.attendance.service.IUserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(value = "/api")
public class CourseController {
    @Autowired
    private ICourseService iCourseService;

    @Autowired
    private IUserInfoService iUserInfoService;

    @RequestMapping(value = "/courseManage")
    public ModelAndView courseManage() {
        ModelAndView modelAndView = new ModelAndView("course-manage");
        List<Course> courseList = iCourseService.selectByExample();
        modelAndView.addObject("courseList" ,courseList);
        return modelAndView;
    }

    @RequestMapping(value = "/courseAdd")
    public ModelAndView courseAdd() {
        ModelAndView modelAndView = new ModelAndView("course-add");
        List<UserInfo> userInfos =  iUserInfoService.selectByRole("任课老师");
        modelAndView.addObject("userInfos", userInfos);
        return modelAndView;
    }

    @PostMapping(value = "/courseAdd")
    public ModelAndView courseAddPost(Course course) {
        ModelAndView modelAndView = new ModelAndView("redirect:/api/courseManage");
        iCourseService.insert(course);
        return modelAndView;
    }
}
