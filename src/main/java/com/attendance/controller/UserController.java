package com.attendance.controller;

import com.attendance.bean.UserInfo;
import com.attendance.service.IUserInfoService;
import com.attendance.utils.ChannelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(value = "/api")
public class UserController {
    @Autowired
    private IUserInfoService iUserInfoService;

    @GetMapping(value = "/userInfoAdd")
    public ModelAndView userInfoAddGet() {
        ModelAndView modelAndView = new ModelAndView("user-info-add");
        return modelAndView;
    }

    @PostMapping(value = "/userInfoAdd")
    public ModelAndView userInfoAddPost(UserInfo userInfo) {
        iUserInfoService.insert(userInfo);
        ModelAndView modelAndView = new ModelAndView("main");
        return modelAndView;
    }

    @GetMapping(value = "/userInfoDel")
    public ModelAndView userInfoDel(@RequestParam  Integer selectFlag) {
        iUserInfoService.deleteByPrimaryKey(selectFlag);
        ModelAndView modelAndView = new ModelAndView("redirect:/api/userInfoManage");
        return modelAndView;
    }

    @RequestMapping(value = "/userInfoManage")
    public ModelAndView userInfoManage() {
        ModelAndView modelAndView = new ModelAndView("user-info-manage");
        List<UserInfo> userInfoList = iUserInfoService.selectByExample();
        modelAndView.addObject("userInfoList", userInfoList);
        return modelAndView;
    }

    @GetMapping(value = "/userInfoModify")
    public ModelAndView userInfoModifyGet(Integer id) {
        ModelAndView modelAndView = new ModelAndView("user-info-modify");
        UserInfo userInfo = iUserInfoService.selectByPrimaryKey(id);
        modelAndView.addObject("userInfo", userInfo);
        return modelAndView;
    }

    @PostMapping(value = "/userInfoModify")
    public ModelAndView userInfoModifyPost(UserInfo userInfo) {
        ModelAndView modelAndView = new ModelAndView("redirect:/api/userInfoManage");
        iUserInfoService.updateByPrimaryKey(userInfo);
        return modelAndView;
    }

    @GetMapping(value = "/passwordModify")
    public ModelAndView pswdModifyGet() {
        ModelAndView view = new ModelAndView("password-modify");
        return view;
    }

    @PostMapping(value = "/passwordModify")
    public ModelAndView pswdModifyPost(String userPassword) {
        UserInfo userInfo = ChannelContext.getUser();
        userInfo.setUserPassword(userPassword);
        iUserInfoService.updateByPrimaryKey(userInfo);
        ModelAndView view = new ModelAndView("main");
        return view;
    }
}
