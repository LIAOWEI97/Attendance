package com.attendance.controller;

import com.attendance.bean.UserInfo;
import com.attendance.service.IUserInfoService;
import com.attendance.utils.ChannelContext;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {
    @Autowired
    private IUserInfoService iUserInfoService;

    @RequestMapping(value = "/login")
    public ModelAndView login(UserInfo userInfo) {
        UserInfo u = iUserInfoService.selectByAccountAndPassword(userInfo);
        ModelAndView modelAndView = null;
        if (u != null) {
            ChannelContext.addUser(u);
            modelAndView = new ModelAndView("main");
        } else {
            modelAndView = new ModelAndView("redirect:/");
        }


        return modelAndView;
    }

    @RequestMapping(value = "/left")
    public ModelAndView left() {
        ModelAndView view = new ModelAndView("left");
        return view;
    }

    @RequestMapping(value = "/center")
    public ModelAndView center() {
        ModelAndView view = new ModelAndView("center");
        return view;
    }

    @RequestMapping(value = "/top")
    public ModelAndView top() {
        ModelAndView view = new ModelAndView("top");
        return view;
    }

    @RequestMapping(value = "/down")
    public ModelAndView down() {
        ModelAndView view = new ModelAndView("down");
        return view;
    }
}
