package com.attendance.controller;

import com.attendance.bean.*;
import com.attendance.service.*;
import com.attendance.utils.ChannelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(value = "/api")
public class StudentController {
    @Autowired
    private IStudentInfoService iStudentInfoService;

    @Autowired
    private IStudentLeaveService iStudentLeaveService;

    @Autowired
    private IClassJoinService iClassJoinService;

    @Autowired
    private IUserInfoService iUserInfoService;

    @Autowired
    private ICourseService iCourseService;

    @Autowired
    private IStudentJoinService iStudentJoinService;

    @RequestMapping(value = "/stuLeaveManage")
    public ModelAndView studentLeaveManage() {
        ModelAndView modelAndView = new ModelAndView("stu-leave-manage");
        List<StudentLeave> studentLeaves = iStudentLeaveService.selectByExample();
        modelAndView.addObject("studentLeaves", studentLeaves);
        return modelAndView;
    }

    @RequestMapping(value = "/stuLeaveAdd")
    public ModelAndView studentLeaveAdd() {
        ModelAndView modelAndView = new ModelAndView("stu-leave-add");
        return modelAndView;
    }

    @PostMapping(value = "/stuLeaveAddPost")
    public ModelAndView studentLeaveAddPost(StudentLeave studentLeave) {
        ModelAndView modelAndView = new ModelAndView("stu-leave-manage");
        UserInfo userInfo = ChannelContext.getUser();
        studentLeave.setLeaveName(userInfo.getUserName());
        iStudentLeaveService.insert(studentLeave);
        List<StudentLeave> studentLeaves = iStudentLeaveService.selectByExample();
        modelAndView.addObject("studentLeaves", studentLeaves);
        return modelAndView;
    }


    @GetMapping(value = "/stuLeaveModify")
    public ModelAndView stuLeaveModify(Integer leaveId) {
        ModelAndView modelAndView = new ModelAndView("stu-leave-modify");
        StudentLeave studentLeave = iStudentLeaveService.selectByPrimaryKey(leaveId);
        modelAndView.addObject("studentLeave", studentLeave);
        return modelAndView;
    }

    @PostMapping(value = "/stuLeaveModify")
    public ModelAndView stuLeaveModifyPost(StudentLeave studentLeave) {
        ModelAndView modelAndView = new ModelAndView("main");
        iStudentLeaveService.updateByPrimaryKey(studentLeave);
        return modelAndView;
    }

    @GetMapping(value = "/studentInfo")
    public ModelAndView studentInfoGet() {
        ModelAndView modelAndView = new ModelAndView("student-info");
        String userAccount = ChannelContext.getUser().getUserAccount();
        Integer studentId = Integer.parseInt(userAccount);
        StudentInfo studentInfo = iStudentInfoService.selectByPrimaryKey(studentId);
        modelAndView.addObject("studentInfo", studentInfo);
        return modelAndView;
    }

    @PostMapping(value = "/studentInfo")
    public ModelAndView studentInfoPost(StudentInfo studentInfo) {
       iStudentInfoService.updateByPrimaryKey(studentInfo);
        ModelAndView modelAndView = new ModelAndView("main");
        return modelAndView;
    }

    @RequestMapping(value = "/clazzJoinManage")
    public ModelAndView clazzJoinManage() {
        ModelAndView modelAndView = new ModelAndView("clazz-join-manage");
        List<ClassJoin> classJoins = iClassJoinService.selectByExample();
        modelAndView.addObject("classJoins", classJoins);
        return modelAndView;
    }

    @RequestMapping(value = "/classJoinAdd")
    public ModelAndView classJoinAdd() {
        ModelAndView modelAndView = new ModelAndView("clazz-join-add");
        return modelAndView;
    }

    @PostMapping(value = "/classJoinAdd")
    public ModelAndView classJoinAddPost(ClassJoin classJoin) {
        ModelAndView modelAndView = new ModelAndView("redirect:/api/clazzJoinManage");
        iClassJoinService.insert(classJoin);
        return modelAndView;
    }

    @PostMapping(value = "/ClazzJoinDel")
    public ModelAndView ClazzJoinDel(Integer classJoinId) {
        ModelAndView modelAndView = new ModelAndView("redirect:/api/clazzJoinManage");
        iClassJoinService.deleteByPrimaryKey(classJoinId);
        return modelAndView;
    }
    @RequestMapping(value = "/stuJoinManage")
    public ModelAndView stuJoinManage() {
        ModelAndView modelAndView = new ModelAndView("stu-join-manage");
        List<StudentJoin> studentJoins = iStudentJoinService.selectByExample();
        modelAndView.addObject("studentJoins", studentJoins);
        return modelAndView;
    }

    @RequestMapping(value = "/studentManage")
    public ModelAndView studentManage() {
        ModelAndView modelAndView = new ModelAndView("student-manage");
        List<StudentInfo> studentInfos = iStudentInfoService.selectByExample();
        modelAndView.addObject("studentInfos", studentInfos);
        return modelAndView;
    }

    @RequestMapping(value = "/stuJoinAdd")
    public ModelAndView stuJoinAdd() {
        ModelAndView modelAndView = new ModelAndView("stu-join-add");
        List<UserInfo> userInfos = iUserInfoService.selectByRole("学生");
        List<Course> courses = iCourseService.selectByExample();
        modelAndView.addObject("userInfos", userInfos);
        modelAndView.addObject("courses", courses);
        return modelAndView;
    }

    @PostMapping(value = "/stuJoinAdd")
    public ModelAndView stuJoinAddPost(StudentJoin studentJoin) {
        ModelAndView modelAndView = new ModelAndView("stu-join-manage");
        iStudentJoinService.insert(studentJoin);
        List<StudentJoin> studentJoins = iStudentJoinService.selectByExample();
        modelAndView.addObject("studentJoins", studentJoins);
        return modelAndView;
    }
}
