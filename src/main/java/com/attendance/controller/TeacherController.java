package com.attendance.controller;

import com.attendance.bean.StudentInfo;
import com.attendance.bean.TeacherInfo;
import com.attendance.bean.UserInfo;
import com.attendance.service.ITeacherService;
import com.attendance.service.IUserInfoService;
import com.attendance.utils.ChannelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(value = "/api")
public class TeacherController {
    @Autowired
    private IUserInfoService iUserInfoService;

    @RequestMapping(value = "/teacherManage")
    public ModelAndView teacherManage() {
        ModelAndView modelAndView = new ModelAndView("teacher-manage");
        List<UserInfo> userInfoList = iUserInfoService.selectByRole("任课老师");
        modelAndView.addObject("userInfoList", userInfoList);
        return modelAndView;
    }

    @RequestMapping(value = "/teacherInfo")
    public ModelAndView teacherInfo() {
        ModelAndView modelAndView = new ModelAndView("teacher-info");
        Integer id = ChannelContext.getUser().getUserId();
        UserInfo userInfo = iUserInfoService.selectByPrimaryKey(id);
        modelAndView.addObject("userInfo", userInfo);
        return modelAndView;
    }

    @PostMapping(value = "/teacherInfo")
    public ModelAndView teacherInfoPost(UserInfo teacherInfo) {
        iUserInfoService.updateByPrimaryKey(teacherInfo);
        ModelAndView modelAndView = new ModelAndView("main");
        return modelAndView;
    }
}
