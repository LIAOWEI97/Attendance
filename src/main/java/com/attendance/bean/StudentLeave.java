package com.attendance.bean;

import java.util.Date;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table student_leave
 */
public class StudentLeave {
    /**
     * Database Column Remarks:
     *   主键
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column student_leave.leave_id
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    private Integer leaveId;

    /**
     * Database Column Remarks:
     *   请假人
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column student_leave.leave_name
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    private String leaveName;

    /**
     * Database Column Remarks:
     *   请假时间
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column student_leave.leave_time
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    private String leaveTime;

    /**
     * Database Column Remarks:
     *   开始时间
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column student_leave.start_time
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    private String startTime;

    /**
     * Database Column Remarks:
     *   请假原因
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column student_leave.leave_reason
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    private String leaveReason;

    /**
     * Database Column Remarks:
     *   请假天数
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column student_leave.leave_day_time
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    private Integer leaveDayTime;

    /**
     * Database Column Remarks:
     *   状态
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column student_leave.leave_status
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    private String leaveStatus;

    /**
     * Database Column Remarks:
     *   处理时间
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column student_leave.dispose_time
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    private String disposeTime;

    /**
     * Database Column Remarks:
     *   处理人
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column student_leave.dispose_people
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    private String disposePeople;

    /**
     * Database Column Remarks:
     *   处理结果
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column student_leave.dispose_result
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    private String disposeResult;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table student_leave
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public StudentLeave(Integer leaveId, String leaveName, String leaveTime, String startTime, String leaveReason, Integer leaveDayTime, String leaveStatus, String disposeTime, String disposePeople, String disposeResult) {
        this.leaveId = leaveId;
        this.leaveName = leaveName;
        this.leaveTime = leaveTime;
        this.startTime = startTime;
        this.leaveReason = leaveReason;
        this.leaveDayTime = leaveDayTime;
        this.leaveStatus = leaveStatus;
        this.disposeTime = disposeTime;
        this.disposePeople = disposePeople;
        this.disposeResult = disposeResult;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table student_leave
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public StudentLeave() {
        super();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column student_leave.leave_id
     *
     * @return the value of student_leave.leave_id
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public Integer getLeaveId() {
        return leaveId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column student_leave.leave_id
     *
     * @param leaveId the value for student_leave.leave_id
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public void setLeaveId(Integer leaveId) {
        this.leaveId = leaveId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column student_leave.leave_name
     *
     * @return the value of student_leave.leave_name
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public String getLeaveName() {
        return leaveName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column student_leave.leave_name
     *
     * @param leaveName the value for student_leave.leave_name
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public void setLeaveName(String leaveName) {
        this.leaveName = leaveName == null ? null : leaveName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column student_leave.leave_time
     *
     * @return the value of student_leave.leave_time
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public String getLeaveTime() {
        return leaveTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column student_leave.leave_time
     *
     * @param leaveTime the value for student_leave.leave_time
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public void setLeaveTime(String leaveTime) {
        this.leaveTime = leaveTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column student_leave.start_time
     *
     * @return the value of student_leave.start_time
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column student_leave.start_time
     *
     * @param startTime the value for student_leave.start_time
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column student_leave.leave_reason
     *
     * @return the value of student_leave.leave_reason
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public String getLeaveReason() {
        return leaveReason;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column student_leave.leave_reason
     *
     * @param leaveReason the value for student_leave.leave_reason
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public void setLeaveReason(String leaveReason) {
        this.leaveReason = leaveReason == null ? null : leaveReason.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column student_leave.leave_day_time
     *
     * @return the value of student_leave.leave_day_time
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public Integer getLeaveDayTime() {
        return leaveDayTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column student_leave.leave_day_time
     *
     * @param leaveDayTime the value for student_leave.leave_day_time
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public void setLeaveDayTime(Integer leaveDayTime) {
        this.leaveDayTime = leaveDayTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column student_leave.leave_status
     *
     * @return the value of student_leave.leave_status
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public String getLeaveStatus() {
        return leaveStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column student_leave.leave_status
     *
     * @param leaveStatus the value for student_leave.leave_status
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public void setLeaveStatus(String leaveStatus) {
        this.leaveStatus = leaveStatus == null ? null : leaveStatus.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column student_leave.dispose_time
     *
     * @return the value of student_leave.dispose_time
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public String getDisposeTime() {
        return disposeTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column student_leave.dispose_time
     *
     * @param disposeTime the value for student_leave.dispose_time
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public void setDisposeTime(String disposeTime) {
        this.disposeTime = disposeTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column student_leave.dispose_people
     *
     * @return the value of student_leave.dispose_people
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public String getDisposePeople() {
        return disposePeople;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column student_leave.dispose_people
     *
     * @param disposePeople the value for student_leave.dispose_people
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public void setDisposePeople(String disposePeople) {
        this.disposePeople = disposePeople == null ? null : disposePeople.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column student_leave.dispose_result
     *
     * @return the value of student_leave.dispose_result
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public String getDisposeResult() {
        return disposeResult;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column student_leave.dispose_result
     *
     * @param disposeResult the value for student_leave.dispose_result
     *
     * @mbg.generated Sun Apr 12 18:01:56 CST 2020
     */
    public void setDisposeResult(String disposeResult) {
        this.disposeResult = disposeResult == null ? null : disposeResult.trim();
    }
}